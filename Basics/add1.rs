fn main() {
	let mut sum = 0.0; // must be mutable
	for i in 0..5 {
		sum += i as f64;
	}
	println!("sum is {}", sum);
}
