// ./args0 'hello world!' frodo 14
// './args0'
// 'hello world!'
// 'frodo'
// '14'
fn main() {
	for arg in std::env::args() {
		println!("'{}'", arg);
	}
}
