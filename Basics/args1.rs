use std::env;

fn main() {
	// idiomatic rust for args
	// expect is unwrap() but allows supplying a message for the panic
	let first = env::args().nth(1).expect("please supply an integer argument");
	// specifying the type of n tell parse what to do
	let n: i32 = first.parse().expect("not an integer!");
	println!("n {}", n);
}
