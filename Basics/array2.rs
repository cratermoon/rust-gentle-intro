fn sum(values: &[i32]) -> i32 {
	let mut res = 0;
	for i in 0..values.len() {
		res += values[i]
	}
	res
}


fn main() {
	let arr = [10,20,30,40];
	// the type of an array includes its slice
	// an array of 3 integers is not the same type as an array of 4 integers
	// the & allows the method to borrow the array as a slice
	// otherwise the method would have to specify the size, because
	let res = sum(&arr);
	println!("sum {}", res);
}
