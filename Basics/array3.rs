fn main() {
	let ints = [1,2,3];
	let floats = [1.1,2.2,3.3];
	let strings = ["hello", "world"];
	let ints_ints = [[1, 2], [10, 20]];

	// the {:?} is Rust's debug, equivalent to Go's %v
	println!("ints {:?}", ints);
	println!("floats {:?}", floats);
	println!("strings {:?}", strings);
	println!("ints_ints {:?}", ints_ints);
}
