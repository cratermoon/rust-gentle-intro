use std::env;
use std::fs::File;
use std::io::Read;

fn main() {
	let first = env::args().nth(1).expect("Please supply a filename");

	let mut file = File::open(&first).expect("unable to open file");

	let mut text = String::new();
	// remember that String is UTF-8, binary files will not work here
	file.read_to_string(&mut text).expect("unable to read file");

	println!("file has {} bytes", text.len());

	// the file will be closed at the end of block
	// when the file variable is dropped
}
