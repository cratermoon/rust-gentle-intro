use std::env;
use std::fs::File;
use std::io::Read;
use std::io;

// simpler error handling using a std::io type alias
// and the ? operator
fn read_to_string(filename: &str) -> io::Result<String> {
	let mut file = File::open(&filename)?;
	let mut text = String::new();
	file.read_to_string(&mut text)?;
	Ok(text)
}

fn main() {
	let file = env::args().nth(1).expect("Please supply a filename");

	let text = read_to_string(&file).expect("bad file, man!");

	println!("file has {} bytes", text.len());

	// the file will be closed at the end of block
	// when the file variable is dropped
}
