fn main() {
	// for loops don't need parens around the condition
	// in this case the condition uses a an iterator,
	// which looks like what other languages call range
	for i in 0..5 {
		println!("Hello, {}!", i);
	}
}
