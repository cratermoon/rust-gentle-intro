fn main() {
	for i in 0..5 {
		// curly brackets are required always
		if i % 2 == 0 {
			println!("even {}", i);
		} else {
			println!("odd {}", i);
		}
	}
}
