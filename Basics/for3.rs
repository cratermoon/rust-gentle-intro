fn main() {
	for i in 0..5 {
		// the if expression, like all expressions in Rust,
		// has a value that can be assigned
		let even_odd = if i % 2 == 0 { "even" } else { "odd" };
		println!("{} {}", even_odd, i);
	}
}
