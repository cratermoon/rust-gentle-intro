fn abs(x: f64) -> f64 {
	if x > 0.0 {
		x
	} else {
		-x
	}
}

fn clamp(x: f64, x1: f64, x2: f64) -> f64 {
	if x < x1 {
		x1
	} else if x > x2 {
		x2
	} else {
		x
	}
}

fn sqr(x: f64) -> f64 {
	x * x
}

fn main() {
	// the Rust compiler will not work out the types of values in function calls
	// the function declaration must specificy the type, and the caller must use
	// the correct type in the argument
	let res = sqr(2.0);
	println!("square is {}", res);
	let res2 = abs(-42.0);
	println!("abs is {}", res2);
	let min = 0.0;
	let max = res2;
	let res3 = clamp(res, min, max);
	println!("clamp {} between {} and {} is {}", res, min, max, res3);
}
