fn modifies(x: &mut f64) {
	*x = 1.0
}

fn main() {
	let mut res = 0.0;
	// You can write Rust functions that modify their arguments.
	// It's not good style, though
	modifies(&mut res);
	println!("res is {}", res);
}
