fn main() {
	// the bang (!) signifies a macro, or syntax extension, in Rust
	println!("Hello, World!")
}
