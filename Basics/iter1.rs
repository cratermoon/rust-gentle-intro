fn main() {
	let mut iter = 0..3;
	// iterators have a next() method that returns an Option
	// It returns None when there are no more elements
	assert_eq!(iter.next(), Some(0));
	assert_eq!(iter.next(), Some(1));
	assert_eq!(iter.next(), Some(2));
	assert_eq!(iter.next(), None);
}
