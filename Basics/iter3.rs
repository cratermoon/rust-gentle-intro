fn main() {
	let arr = [10,20,30];

	// you can't do for i in arr,
	// you must use the iter() method
	for i in arr.iter() {
		println!("{}", i);
	}

	// but slices are implicitly iterators
	let slice = &arr;
	for i in slice {
		println!("{}", i);
	}
}
