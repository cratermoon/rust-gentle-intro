fn main() {
	// a variable. Actually not variable, because it's not mutable
	let answer = 42;
	println!("Hello, {}", answer);
}
