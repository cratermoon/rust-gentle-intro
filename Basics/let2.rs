fn main() {
	let answer = 42;
	// assertions are built into the language
	assert_eq!(answer, 14);
}
