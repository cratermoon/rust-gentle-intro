// a Result 'box' has two compartments.
// The first, 'Ok' is defined as an i32
// The seconed, 'Err', is defined as a String
// The actual definition is an enum Result<T, E>
// so the two component types can be anything
fn good_or_bad(good: bool) -> Result<i32,String> {
	if good {
		Ok(42)
	} else {
		Err("bad".to_string())
	}
}

fn main() {
	// will print '42'
	println!("{:?}", good_or_bad(true));
	// will print 'bad'
	println!("{:?}", good_or_bad(false));

	// prints 'Cool, got 42'
	match good_or_bad(true) {
		Ok(n) => println!("Cool, got {}", n),
		Err(e) => println!("Oops, got {}", e)
	}
}
