fn main() {
	let ints = [1,2,3,4,5];
	let slice = &ints;
	// calling int[i] if i is out of bounds causes a panic
	// get returns an Optional.
	let first = slice.get(0);
	// if the index it out of bounds, it doesn't panic
	// but instead returns None
	let maybe_last = slice.get(5);

	println!("first {:?}", first);
	println!("maybe_last {:?}", maybe_last);

	// unwrap_or allows providing a value if the Optional
	// holds a None. Notice the &
	// get always returns a reference,
	// so the type of the value must be a reference
	let last = *slice.get(5).unwrap_or(&-1);

	println!("last {}", last); 
}
