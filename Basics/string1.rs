fn dump(s: &str) {
	println!("str {}", s);
}

fn main() {
	// there are two types of strings in Rust
	// string literals like so are static
	// the type is string slice
	let text = "hello dolly";
	let s = text.to_string();

	dump(text);
	dump(&s);
}
