fn main() {
	// slice notation works for strings, too
	let text = "static";
	let string = "dynamic".to_string();

	let text_s = &text[1..];
	let string_s = &string[2..4];

	println!("slices text {} string {}", text_s, string_s);
}
