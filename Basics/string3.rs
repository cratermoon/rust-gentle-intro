fn main() {
	let multilingual = "Hi! ¡Hola! привет!";

	// you can iterate over the characters in a string
	for ch in multilingual.chars() {
		print!("'{}' ", ch);
	}
	println!("");
	// UTF-8
	// len is the number of bytes
	println!("len {}", multilingual.len());
	// but there are only 18 characters
	println!("count {}", multilingual.chars().count());
	
	// but you can't index 
	let not_hi = &multilingual[11..];
	println!("not Russian hi '{}'", not_hi);

	let maybe = multilingual.find('п');
	if maybe.is_some() {
		let hi = &multilingual[maybe.unwrap()..];
		println!("Russian hi '{}'", hi);
	}
}
