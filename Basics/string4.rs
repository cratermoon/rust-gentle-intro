fn main() {
	let multilingual = "Hi! ¡Hola! привет!";
	
	// replace if block in string3.rs with
	// idiomatic use of match
	match multilingual.find('п') {
		Some(idx) => {
			let hi = &multilingual[idx..];
			println!("Russian hi {}", hi);
		},
		None => println!("Couldn't find greeting, Товарищ")
	};

	// or also
	if let Some(idx) = multilingual.find('п') {
		println!("Russian hi {}", &multilingual[idx..])
	}
}
