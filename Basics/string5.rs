fn main() {
	let mut s = String::new();
	// A String is basically a Vec of UTF-8 chars (Vec<u8>
	s.push('H');
	s.push_str("ello,");
	s.push(' ');
	// syntactic sugar for push_str
	s += "World!";
	s.pop();

	assert_eq!(s, "Hello, World");
}
