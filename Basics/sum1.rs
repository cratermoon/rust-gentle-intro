fn main() {
	// idiomatic way of summing 
	let sum: i32 = (0..5).sum();

	println!("sum was {}", sum);
	// again, arrays need iter()
	let sum: i64 = [10, 20, 30].iter().sum();
	println!("sum was {}", sum);
}
