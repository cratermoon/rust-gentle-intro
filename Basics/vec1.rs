fn main() {
	// Vec is a resizable array
	let mut v = Vec::new();
	// you can push things on the end,
	// it will grow as needed
	// default capacity is 0
	// as of this writing, there are no guarantees
	// on how growth is done, and the implementation may change
	v.push(10);
	v.push(20);
	v.push(30);

	let first = v[0];
	let maybe_first = v.get(0);

	println!("v is {:?}", v);
	println!("first is {}", first);
	println!("maybe_first is {:?}", maybe_first);
}
