fn main() {
	// the macro vec! is syntactic sugar to initalize a Vec
	let mut v1 = vec![10, 20, 30, 40];
	// pop removes the *last* value
	v1.pop();

	let mut v2 = Vec::new();
	v2.push(10);
	v2.push(20);
	v2.push(30);

	// this compares by value
	assert_eq!(v1, v2);

	v2.extend(0..2);
	// now v2 has 5 elements
	assert_eq!(v2, &[10,20,30,0,1]);
	assert_eq!(v2.len(), 5);
}
