fn main() {
	let mut v1 = vec![1, 10, 5, 1, 2, 11, 2, 40];
	// a Vec can be sorted
	v1.sort();
	assert_eq!(v1, &[1, 1, 2, 2, 5, 10, 11, 40]);
	// and dupes removed
	v1.dedup();
	assert_eq!(v1, &[1, 2, 5, 10, 11, 40]);
}
