use std::env;
use std::fs::File;
use std::io;
use std::io::prelude::*;

fn read_all_lines(filename: &str) -> io::Result<i32> {
	let file = File::open(&filename)?;

	let reader = io::BufReader::new(file);

	let mut line_count = 0;

	for line in reader.lines() {
		line_count += 1;
		let line = line?;
		println!("{}", line);
	}
	Ok(line_count)
}

fn main() {
	let file = env::args().nth(1).expect("Please supply a filename");

	match read_all_lines(&file) {
		Ok(c) => println!("Cool, read {} lines", c),
		Err(e) => println!("Oops, got {}", e)
	}

}

