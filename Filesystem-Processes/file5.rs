// file5.rs
use std::env;
use std::fs::File;
use std::io;
use std::io::prelude::*;

// we don't specify the type of R here because
// io::BufReader doesn't specify
struct Lines<R> {
    reader: io::BufReader<R>,
    buf: String,
    count: i32
}

// This just says the the Lines struct type parameter R
// is something that has the std::io:Read trait
impl<R: Read> Lines<R> {
    fn new(r: R) -> Lines<R> {
        Lines{reader: io::BufReader::new(r), buf: String::new(), count: 0}
    }

    // I *think* this tells the compiler that the lifetimes
    // of the method and the result are the same
    fn next<'a>(&'a mut self) -> Option<io::Result<&'a str>> {
        self.buf.clear();
        match self.reader.read_line(&mut self.buf) {
            Ok(nbytes) => {
                if nbytes == 0 {
                    None
                } else {
                    self.count += 1;
                    let line = self.buf.trim_end();
                    Some(Ok(line))
                }
            }
            Err(e) => Some(Err(e)),
        }
    }
}

fn read_all_lines(filename: &str) -> io::Result<i32> {
    let file = File::open(&filename)?;

    let mut lines = Lines::new(file);
    while let Some(line) = lines.next() {
        let line = line?;
        println!("{}", line);
    }

    Ok(lines.count)
}

fn main() {
    let file = env::args().nth(1).expect("Please supply a filename");

    match read_all_lines(&file) {
        Ok(n) => println!("Cool, read {} lines", n),
        Err(e) => println!("Oops, got {}", e),
    }
}
