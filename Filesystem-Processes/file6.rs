// file6.rs
use std::env;
use std::fs::File;
use std::io;
use std::io::prelude::*;

// the io::Result<()> construction seems idiomatic
// https://newbedev.com/rust/std/io/type.result says,
// "A specialized Result type for I/O operations.
// This type is broadly used across std::io 
// for any operation which may produce an error."
fn write_out(f: &str) -> io::Result<()> {
    // the question mark operator,
    // short-circuits. If the expression results in
    // an error, the function immediately returns
    let mut out = File::create(f)?;
    // out is dropped on exit, which closes the file, too
    write!(out, "answer is {}\n", 42)?;
    Ok(())
}

fn main() {
    let file = env::args().nth(1).expect("Please supply a filename");

    write_out(&file).expect("write failed");
}