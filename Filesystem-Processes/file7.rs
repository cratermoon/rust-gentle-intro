// file7.rs
use std::env;
use std::path::PathBuf;

fn main() {
    // rust compiler complains about home_dir
    //  Deprecated since 1.29.0
    let home = env::home_dir().expect("homeless!");
    let mut path = PathBuf::new();
    path.push(home);
    path.push(".cargo");

    if path.is_dir() {
        println!("{}", path.display());
    }
}