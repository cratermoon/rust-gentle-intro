// file8.rs
use std::env;

fn main() {
    let mut path = env::current_dir().expect("Can't access current directory");
    // is this really idiomatic?
    loop {
        println!("{}", path.display());
        if !path.pop() {
            break;
        }
    }
}