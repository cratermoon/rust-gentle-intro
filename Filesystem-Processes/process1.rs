// process1.rs
use std::process::Command;

fn main() {
    // PATH-aware
    let status = Command::new("rustc")
        .arg("-V")
        .status()
        .expect("no rustc?");

    println!("cool {} code {}", status.success(), status.code().unwrap());
}