// process2.rs
use std::process::Command;

fn main() {
    // PATH-aware
    let output = Command::new("rustc")
        .arg("-V")
        // captures the output, both stdout and stderr
        .output()
        .expect("no rustc?");

    if output.status.success() {
        println!("ok!");
    }
    println!("stdout {} stderr {}", output.stdout.len(), output.stderr.len());
}