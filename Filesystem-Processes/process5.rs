// process5.rs
use std::process::{Command,Stdio};

fn main() {
    // fork-like
    let mut child = Command::new("rustc")
        .arg("-V")
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .spawn()
        .expect("no rustc?");

    let res = child.wait();
    //res is a Result(ExitStatus)
    println!("res {:?}", res);
}