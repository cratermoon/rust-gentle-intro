// mod3.rs
// the module name is the filename
mod foo;
mod boo;

fn main() {
    let f = foo::Foo::new("hello");
    let res = boo::answer();
    println!("{:?} {}", f, res);
    let q = boo::bar::question();
    println!("{}", q);
}