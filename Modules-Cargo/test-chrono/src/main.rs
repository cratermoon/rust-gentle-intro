extern crate chrono;
use chrono::*;

fn main() {
    let date = Local.ymd(2010,3,14);
    println!("date {}", date);

    let bad_date = Local.ymd_opt(2021,2,31);
    match bad_date.single() {
        Some(x) => println!("date {}", x),
        None => println!("bad date"),
    };
    
}
