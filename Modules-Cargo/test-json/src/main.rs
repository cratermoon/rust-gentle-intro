extern crate json;


fn main() {
    // r# is a raw string literal
    let mut doc = json::parse(r#"
    {
        "code": 200,
        "success": true,
        "payload": {
            "features": [
                "awesome",
                "easyAPI",
                "lowLearningCurve"
            ]
        }
    }
    "#).expect("parse failed");

    println!("debug {:?}", doc);
    println!("display {}", doc);

    let code = doc["code"].as_u32().unwrap_or(0);
    let success = doc["success"].as_bool().unwrap_or(false);

    assert_eq!(code, 200);
    assert_eq!(success, true);

    let features = &mut doc["payload"]["features"];
    features.push("cargo!").expect("couldn't push");
    for v in features.members() {
        println!("{}", v.as_str().unwrap());
    }
}
