#[macro_use]
extern crate serde_derive;
extern crate serde_json;

#[derive(Serialize, Deserialize, Debug)]
struct Person {
    name: String,
    age: u8,
    address: Address,
    phones: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug)]
struct Address {
    street: String,
    city: String,
}

fn main() {
    let data = r#"{
        "name": "Ralf Cramden", "age": 43,
        "address": {"street": "w. addison", "city": "Chicago"},
        "phones": ["8675309"]
    }
    "#;

    let p: Person = serde_json::from_str(data).expect("deserialize error");
    println!("Please call {} at {}", p.name, p.phones[0]);

    println!("{:#?}", p);
}
