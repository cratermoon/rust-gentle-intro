// closure1.rs
fn main() {
    // closure
    let f = |x| x * x;

    let res = f(10);
    println!("{}", res);

    // but now we've told rust that f is integral
    // this doesn't compile
    // let res2 = f(3.14159)

    let m = 2.0;
    let c = 1.0;

    // note that the closure can use m and c
    // or "close over"
    let lin = |x| m*x + c;

    // internally, a closure is a struct the borrows
    // values from elsewhere
    println!("res {} {}", lin(1.0), lin(2.0));

}