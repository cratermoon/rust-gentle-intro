fn main() {
    let mut s = "world";
    // A Gentle Introduction To Rust says we need the braces
    // but apparently that's not (no longer?) true
    //    {
    let mut changer = || s = "world";

    changer();
    //    }

    assert_eq!(s, "world");
}
