// enum1.rs 
// PartialEq allows us to compare Directions
#[derive(Debug, PartialEq)]
enum Direction {
    Up,
    Down,
    Left,
    Right
}

// enums can have impls
impl Direction {
    fn as_str(&self) -> &'static str {
        // we have to dereference &Direction to Direction
        match *self {
            Direction::Up => "up",
            Direction::Down => "down",
            Direction::Left => "left",
            Direction::Right => "right"
        }
    }

    // there's no ordering, but we make one up
    fn next(&self) -> Direction {
        use Direction::*;
        match *self {
            Up => Right,
            Right => Down,
            Down => Left,
            Left => Up
        }
    }
}

fn main() {
    let start = Direction::Left;
    println!("start {:?}", start);
    // why as_str then? it doesn't allocate
    println!("start {}", start.as_str());

    // comparison here depends on enum deriving from PartialEq
    assert_eq!(start, Direction::Left);

    let mut d = start;
    for _ in 0..8 {
        println!("d {}", d.as_str());
        d = d.next();
    }
}