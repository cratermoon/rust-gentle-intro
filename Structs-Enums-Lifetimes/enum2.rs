// enum2.rs 
enum Speed {
    Slow = 10,
    Medium = 20,
    Fast = 30
}

#[derive(PartialEq,PartialOrd)]
enum Difficulty {
    Easy = 1,
    Medium, // auto increment to 2
    Hard // 3
}
fn main() {
    let s = Speed::Slow;
    // rust type case
    let speed = s as u32;
    println!("{}", speed);

    let d = Difficulty::Medium;
    let diff = d as u32;
    println!("{}", diff);

    // enabled by #[derive(PartialEq,PartialOrd)]
    // PartialOrd requires PartialEq
    assert!(Difficulty::Easy < Difficulty::Medium);
}