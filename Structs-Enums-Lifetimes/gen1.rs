// generics

// T is the type
// T::Output tells the compiler that the result of the
// method is the same T as the parameter type T
fn sqr<T> (x: T) -> T::Output
// where keyword tells the compiler the constraints for type T
// in this case, that it can be multiplied and copied
where T: std::ops::Mul + Copy {
    x * x
}

fn main() {
    let res = sqr(10.0);
    println!("{}", res);

    // the following wouldn't compile because &str
    // doesn't implement trail Mul
    // let res2 = sqr("ten");
}