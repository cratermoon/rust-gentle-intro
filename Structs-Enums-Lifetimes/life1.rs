// life1.rs

#[derive(Debug)]
struct A {
    s: &'static str
}

fn main() {
    let a = A {
        s: "hello failure"
    };

    println!("{:?}", a);
}