// life3.rs

#[derive(Debug)]
struct A<'a> {
    // lifetime a
    s: &'a str, // also lifetime a
}

fn main() {
    let s = "I'm a little string".to_string();

    let a = A { s: &s };

    println!("{:?}", a);
}
