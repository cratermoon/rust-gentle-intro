#[derive(Debug)]
struct Point {
    x: f32,
    y: f32
}

fn match_tuple(t: (i32, String)) {
    let text = match t {
        (0, s) => format!("zero {}", s),
        (1, ref s) if s == "hello" => format!("hello one"),
        tt => format!("no match")
    };
    println!("{}", text);
}
// match1.rs 
fn main() {
    let t = (10, "hello".to_string());
    // destructuring
    let (n,s) = t;

    println!("t {} s {}", n, s);

    let p = Point{x:1.0,y:2.0};

    //this works as with tuples
    let Point{x,y} = p;

    // x and y are copied, so p still lives
    println!("p {:?}", p);

    match_tuple((0,"answer".to_string()));
    match_tuple((1,"hello".to_string()));

    // &str
    match (42,"answer") {
        (42,"answer") => println!("yes"),
        _ => println!("no")
    };

    let ot = Some((2, "hello".to_string()));

    if let Some((_, ref s)) = ot {
        assert_eq!(s, "hello");
    }
}