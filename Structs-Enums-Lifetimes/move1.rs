fn main() {
	let s1 = "hello, weird".to_string();
	let s2 = s1;
	// this will fail to even compile, with a use of moved error
	println!("s1 {}", s1);
}
