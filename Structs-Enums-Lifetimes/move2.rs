fn dump(s: &str) {
    println!("{}", s)
}

fn main() {
	let s1 = "hello, weird".to_string();
	// this will fail to even compile, with a use of moved error
	dump(&s1);
    println!("s1 {}", s1);
    dump("Hello, world!");
}
