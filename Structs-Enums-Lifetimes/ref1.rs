// ref1.rs
fn main() {
    let s1 = "hello, dolly".to_string();
    let mut rs1 = &s1;
    {
        let tmp = "hello, world".to_string();
        rs1 = &tmp;
    }
    // this won't compile because rs1 borrowed tmp
    // tmp no longer exists
    // nope, rs1 does not revert back to a borrow of s1
    println!("rs1 {}", rs1);
}