// struct1.rs
struct Person {
    given_name: String,
    surname: String
}

fn main() {
    let p = Person {
        given_name: "Steven".to_string(),
        surname: "Newton".to_string()
    };

    println!("person {} {}", p.given_name, p.surname);
}