
// struct2.rs
struct Person {
    given_name: String,
    surname: String
}

impl Person {
    fn new(given: &str, surname: &str) -> Person {
        Person {
            given_name: given.to_string(),
            surname: surname.to_string()
        }
    }
}

fn main() {
    let p = Person::new("Steven","Newton");

    println!("person {} {}", p.given_name, p.surname);
}
