
// this gives Person the ability to be used in {:?} format strings
#[derive(Debug)]
struct Person {
    given_name: String,
    surname: String
}

impl Person {
    fn new(given: &str, surname: &str) -> Person {
        Person {
            given_name: given.to_string(),
            surname: surname.to_string()
        }
    }

    // pronounced "reference self"
    fn full_name(&self) -> String {
        format!("{} {}", self.given_name, self.surname)
    }

    // the keyword Self can be read as the type of the struct
    // this method is shown the tutorial but never used ¯\_(ツ)_/¯
    fn copy(&self) -> Self {
        Self::new(&self.given_name, &self.surname)
    }

    fn set_given_name(&mut self, given: &str) {
        self.given_name = given.to_string();
    }

    fn to_tuple(self) -> (String, String) {
        (self.given_name, self.surname)
    }
}

fn main() {
    let mut p = Person::new("Steven","Newton");

    println!("{:?}", p);

    p.set_given_name("Steve");

    println!("{:?}", p);
    println!("person {}", p.full_name());

    // I made up my own usage
    let p2 = p.copy();

    println!("{:?}", p2.to_tuple());

    // now the variables given_name and surname exist
    let Person { given_name, surname } = p;

    // magic!?
    println!("{} {}", given_name, surname);
    println!("{}", given_name.len());

}
