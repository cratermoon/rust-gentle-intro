use std::f64::consts;
// trait3.rs
// implementing Iterator for a floating-point range


struct FRange {
    val: f64,
    end: f64,
    incr: f64
}


// creates an FRange from x1 to x2 with increment skip
fn range(x1: f64, x2: f64, skip: f64) -> FRange {
    FRange {val: x1, end: x2, incr: skip}
}

// syntax is "impl Trait for Type"
impl Iterator for FRange {
    type Item = f64;

    // trait only defines one method
    fn next(&mut self) -> Option<Self::Item> {
        let res = self.val;
        if res >= self.end {
            None
        } else {
            self.val += self.incr;
            Some(res)
        }
    }
}

fn main() {
    for x in range(0.0, 1.0, 0.1) {
        // this fmt string specifies one decimal place precision
        println!("{:.1}", x);
    }

    for y in range(0.0, 360.0, 360.0 / (2.0 * consts::PI)) {
        println!("{:.1}", y);
    }

    // bonus math
    let v: Vec<f64> = range(0.0, 1.0, 0.1).map(|x| x.sin()).collect();
    println!("{:?}", v);

    let s = &v[1..];

    for n in s {
        println!("{:.1}", n);
    }
}