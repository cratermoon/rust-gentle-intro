fn main() {

    // tuples can have fields of different types
    let tuple = ("hello", 5, 'c');

    assert_eq!(tuple.0, "hello");
    assert_eq!(tuple.1, 5);
    assert_eq!(tuple.2, 'c');

    for t in ["zero", "one", "two"].iter().enumerate() {
        // enumerate is a generator which returns pairs
        // (i, val)
        println!("i {} val {}", t.0, t.1);
    }

    // zip combines iterators into an iterator of tuples
    let names = ["ten", "hundred", "thousand"];
    let nums = [10, 100, 1000];
    let powers = [1, 2, 3];
    // zip of zip results in a tuple with
    // element (i, (tuple(i, val)))
    for p in names.iter().zip(nums.iter().zip(powers.iter())) {
        print!(" i \"{}\" val {}  power {}", p.0, p.1.0, p.1.1);
    }
    println!("");
}